# What I did and how

## Learn Vue.js

I learned the basics and the template syntax, and I have to say that Vue is easier to learn than React and is fun to use !

## Learn Vuex

Having already worked with Redux, using Vuex was a breeze.

As a general rule I don’t overuse the store.

When having to decide if I should store data to Vuex I use this diagram:

![Should I store this data to vuex](vuex-or-not.webp)

I also used namespaced stores for modules (users).

## Code organisation

I decided to split the app by notions.

Since there is only one notion in this app I just created the User module.

I created the core folder for common functionalities like the FlexLayout component whith horizontal or vertical direction as a prop.

## Components structure

I made a distinction between the smart components (Pages folder) and the dumbs components (Components folder).

In a nutshell:

- smart components: can access the store and the router
- dumbs components: take props and emits events

## Read the challenge

After reading the challenge I decided to use Vuetify because I used React Material-UI in the past.

## Create the components

I first created the `User` dumb card component then the `CardsUsers` smart component connected to the store.

Then I created the `UserCreator` for switching from the + button to the `UserForm` component (Calls scrollIntoView on mount to be sure it is always visible).

## Write the integration tests

I decided to tests the components with Vue Testing Library.

It provides a solution to test Vue.js applications, but from a user point of view. According to the doc :

> The more your tests resemble the way your software is used, the more confidence they can give you.

I used SWR to create a mock server that intercepts all requests and handle it just like you would if it were a real server.

This makes the tests fast and easy to write.

The cool thing about MSW is that you can also use all the exact same "server handlers" in the browser during development as well.

I decided to write a single integration test of the entire app since it's a SPA that uses all the components in one place.

I tested the cards view crud, the grid view crud and the case of network error.

As a general rule, this is what to test and how to do it :

- Components: Low priority, easy to do. Write unit tests for each component.
- Pages: High priority, hard to do. You have to mock api calls.
- Routes: Usually bugs aren’t here. Leave it for E2E tests.
- Services:
- api interface: I personnally don’t test this part (90% of the code is mocked).
- helpers/dataFormaters: High priority, easy to do. Usually, the easiest tests to do in your app!
- Store: The hardest part to test. You can test it via integration tests. Testing action, getter and initial state separately is useless.

## Write end to end tests

Since the integration tests covers all the app functionalities, I didn't find useful to write a similar end to end test.

But I have problems to interact with Vuetify Dialog with Vue testing library, so I decided to test the edit user feature in cypress.

I used Miragejs to mock the server API because I couldn't figure out how to setup MSW with cypress.
