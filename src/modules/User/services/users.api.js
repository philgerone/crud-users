import axios from "axios";
// import { members } from "../store/users";

export const fetchUsers = async function() {
  // if (window.Cypress) {
  //   return members;
  // }
  const response = await axios.get("/users");
  return response.data.users;
};
