import { fetchUsers } from "../services/users.api";

const actions = {
  addUser({ commit }, user) {
    commit("addUser", user);
  },
  updateUser({ commit }, user) {
    commit("updateUser", user);
  },
  removeUser({ commit }, id) {
    commit("removeUser", id);
  },
  async getAllUsers({ commit }) {
    try {
      commit("loading");
      const users = await fetchUsers();

      commit("fetchSuccess", users);
    } catch (error) {
      commit("fetchError", error);
    }
  }
};

export default actions;
