const mutations = {
  addUser(state, { name, role, city }) {
    const id = new Date().getTime();
    state.items.push({ id, name, role, city });
  },
  updateUser(state, { id, name, role, city }) {
    const user = state.items.find((user) => user.id === id);
    user.name = name;
    user.role = role;
    user.city = city;
  },
  removeUser(state, id) {
    const index = state.items.findIndex((user) => user.id === id);
    state.items.splice(index, 1);
  },
  fetchSuccess(state, users) {
    state.loading = false;
    state.items = users;
    state.errorMessage = null;
  },
  fetchError(state, error) {
    state.loading = false;
    state.errorMessage = error.message;
  },
  loading(state) {
    state.loading = true;
  }
};

export default mutations;
