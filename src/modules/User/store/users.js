import { ROLES } from "../../../core/types";
import actions from "./actions";
import mutations from "./mutations";

export const members = [
  { id: 1, name: "John Doe", role: ROLES[0], city: "Paris" },
  { id: 2, name: "Sponge Bob", role: ROLES[1], city: "Lyon" },
  { id: 3, name: "Daniel Tiger", role: ROLES[2], city: "NYC" },
  { id: 4, name: "Foo Bar", role: ROLES[0], city: "NYC" },
  { id: 5, name: "James Bond", role: ROLES[1], city: "London" }
];

const state = () => ({
  items: [],
  loading: false,
  errorMessage: null
});

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
