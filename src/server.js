import { createServer } from "miragejs";
import { members } from "./modules/User/store/users";

export function makeServer({ environment = "development" } = {}) {
  let server = createServer({
    environment,

    routes() {
      this.get("/users", () => {
        return {
          users: members
        };
      });
    }
  });

  return server;
}
