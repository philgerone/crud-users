import Vue from "vue";
import Vuex from "vuex";

import users from "../modules/User/store/users";

import mutations from "./mutations";
import actions from "./actions";

Vue.use(Vuex);

export function getDefaultStore() {
  return {
    state: {},
    mutations,
    actions,
    modules: {
      users
    }
  };
}

export default new Vuex.Store(getDefaultStore());
