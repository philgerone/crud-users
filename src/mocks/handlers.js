import { rest } from "msw";
import { members } from "../modules/User/store/users";

export default [
  rest.get("/users", (req, res, ctx) => {
    return res(
      ctx.json({
        users: members
      })
    );
  })
];
