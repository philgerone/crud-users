# users-crud

An application to manage team members.

- Each team member has an id, name, role and city
- Team members should be represented using cards or in a grid
- You can create, update and remove team members
- Add feature behavior:
  - A + button is displayed after all members cards
  - Clicking on that button will transform it into a form card
  - Form card can be submitted or cancelled (bringing back the initial state)
  - Upon validation, a new card is added and the + button comes back

## Project setup

```
yarn install
yarn msw:setup
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Run your end-to-end tests

```
yarn test:e2e

Run in headless mode without GUI :
yarn test:e2e --headless

```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
