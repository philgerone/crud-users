// https://docs.cypress.io/api/introduction/api.html

import { makeServer } from "../../../src/server";

describe("Crud users", () => {
  let server;

  beforeEach(() => {
    server = makeServer({ environment: "test" });
  });

  afterEach(() => {
    server.shutdown();
  });

  it("Visits the app root url", () => {
    cy.visit("/");

    cy.findByText("John Doe");

    cy.findAllByRole("button", {
      name: /update/i
    })
      .eq(0)
      .click();

    cy.findByLabelText(/name/i).type("EEE");

    // after clicking on close
    cy.findAllByRole("button", { name: /close/i }).click();
    // whe should have the same state as before
    cy.findByText("John Doe");

    cy.findAllByRole("button", {
      name: /update/i
    })
      .eq(0)
      .click();

    cy.findByLabelText(/name/i).type("{selectall}John Wayne");

    cy.findAllByRole("button", { name: /save/i }).click();
    cy.findByText("John Doe").should("not.exist");
    cy.findByText("John Wayne");
  });
});
