import Vue from "vue";
import "@testing-library/jest-dom";
import { screen, waitFor } from "@testing-library/vue";
import userEvent from "@testing-library/user-event";
import { setupServer } from "msw/node";
import { rest } from "msw";

import App from "../../src/App";
import handlers from "../../src/mocks/handlers";
import { render } from "../render";

const server = setupServer(...handlers);

/**
 * Adds a user by clicking on the + button,
 * fills the form and waits for the form to disappear
 */
async function addUser() {
  userEvent.click(screen.getByLabelText("add user"));

  expect(await screen.findByText(/new user/i)).toBeInTheDocument();

  expect(
    await screen.findByRole("button", { name: /validate/i })
  ).toBeDisabled();

  let input = screen.getByLabelText(/name/i);
  userEvent.type(input, "Philippe");
  expect(input).toHaveValue("Philippe");

  input = screen.getByLabelText(/role/i);
  // expect(screen.getByText(ROLES[0])).toBeInTheDocument();

  input = screen.getByLabelText(/city/i);
  userEvent.type(input, "Saint-Max");
  expect(input).toHaveValue("Saint-Max");

  await Vue.nextTick();

  expect(
    await screen.findByRole("button", { name: /validate/i })
  ).not.toBeDisabled();

  userEvent.click(screen.getByRole("button", { name: /validate/i }));

  // wait for disappearance
  await waitFor(() => {
    expect(screen.queryByText(/new user/i)).not.toBeInTheDocument();
  });
}

beforeAll(() => {
  server.listen();
});

afterAll(() => {
  server.close();
});

describe("Cards view", () => {
  it("should display the users in cards", async () => {
    render(App);

    // await checkCardsUsers();

    // each card has a "list-item" role
    expect(await screen.findAllByRole("list-item")).toHaveLength(5);

    // ADD
    await addUser();

    expect(await screen.findAllByRole("list-item")).toHaveLength(6);

    // DELETE

    // click on 1st user remove button
    const firstRemoveButton = screen.getAllByRole("button", {
      name: /remove/i
    })[0];
    userEvent.click(firstRemoveButton);

    // wait for disappearance
    await waitFor(() => {
      expect(screen.queryByText("John Doe")).not.toBeInTheDocument();
    });

    expect(screen.getAllByRole("list-item")).toHaveLength(5);

    // TODO check why the dialog is not detected

    // UPDATE
    // click on 1st user update button
    // let firstUpdateButton = screen.getAllByRole("button", {
    //   name: /update/i
    // })[0];
    // userEvent.click(firstUpdateButton);

    // // check dialog appearance
    // expect(await screen.findByText(/user edition/i)).toBeInTheDocument();

    // // change name value
    // let input = screen.getByLabelText(/name/i);
    // userEvent.type(input, "{selectall}John Wayne");
    // expect(input).toHaveValue("John Wayne");

    // // check close button : name should not have changed
    // userEvent.click(screen.getByRole("button", { name: /close/i }));
    // expect(await screen.findByText("John Doe")).toBeInTheDocument();
    // expect(screen.queryByText("John Wayne")).not.toBeInTheDocument();

    // // click on 1st user update button
    // firstUpdateButton = screen.getAllByRole("button", {
    //   name: /update/i
    // })[0];
    // userEvent.click(firstUpdateButton);

    // // check dialog appearance
    // expect(screen.getByText(/user edition/i)).toBeInTheDocument();

    // input = screen.getByLabelText(/name/i);
    // userEvent.type(input, "{selectall}John Wayne");
    // expect(input).toHaveValue("John Wayne");

    // userEvent.click(screen.getByRole("button", { name: /save/i }));

    // expect(await screen.findByText("John Wayne")).toBeInTheDocument();
  });

  it("should display a message if there is a network error", async () => {
    // mock 500 response for users
    server.use(
      rest.get("/users", (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    render(App);

    expect(
      await screen.findByText("Request failed with status code 500")
    ).toBeInTheDocument();
  });
});
