import Vue from "vue";
import "@testing-library/jest-dom";
import { screen, waitFor, within } from "@testing-library/vue";
import userEvent from "@testing-library/user-event";
import { setupServer } from "msw/node";
import { rest } from "msw";

import App from "../../src/App";
import handlers from "../../src/mocks/handlers";
import { render } from "../render";

const server = setupServer(...handlers);

/**
 * Checks table view users
 * @param {number} users number
 */
async function checkTableUsers(usersLength) {
  expect(await screen.findByRole("table")).toBeInTheDocument();
  // head + body
  expect(screen.getAllByRole("rowgroup")).toHaveLength(2);
  expect(screen.getAllByRole("columnheader")).toHaveLength(4);

  expect(
    within(screen.getAllByRole("rowgroup")[1]).getAllByRole("row")
  ).toHaveLength(usersLength);
}

/**
 * Adds a user by clicking on the + button,
 * fills the form and waits for the form to disappear
 */
async function addUser() {
  userEvent.click(screen.getByLabelText("add user"));

  expect(await screen.findByText(/new user/i)).toBeInTheDocument();

  expect(
    await screen.findByRole("button", { name: /validate/i })
  ).toBeDisabled();

  let input = screen.getByLabelText(/name/i);
  userEvent.type(input, "Philippe");
  expect(input).toHaveValue("Philippe");

  input = screen.getByLabelText(/role/i);
  // expect(screen.getByText(ROLES[0])).toBeInTheDocument();

  input = screen.getByLabelText(/city/i);
  userEvent.type(input, "Saint-Max");
  expect(input).toHaveValue("Saint-Max");

  await Vue.nextTick();

  expect(
    await screen.findByRole("button", { name: /validate/i })
  ).not.toBeDisabled();

  userEvent.click(screen.getByRole("button", { name: /validate/i }));

  // wait for disappearance
  await waitFor(() => {
    expect(screen.queryByText(/new user/i)).not.toBeInTheDocument();
  });
}

beforeAll(() => {
  server.listen();
});

afterAll(() => {
  server.close();
});

describe("Table view", () => {
  it("should display the users in a table view", async () => {
    render(App);

    userEvent.click(screen.getByLabelText("table view"));

    await checkTableUsers(5);

    // ADD

    await addUser();

    await checkTableUsers(6);

    // DELETE

    // click on 1st user remove button
    const firstRemoveButton = screen.getAllByLabelText("delete user")[0];
    userEvent.click(firstRemoveButton);

    // wait for disappearance
    await waitFor(() => {
      expect(screen.queryByText("John Doe")).not.toBeInTheDocument();
    });

    await checkTableUsers(5);
  });

  it("should display a message if there is a network error", async () => {
    // mock 500 response for users
    server.use(
      rest.get("/users", (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );

    render(App);

    expect(
      await screen.findByText("Request failed with status code 500")
    ).toBeInTheDocument();
  });
});
